package equity.id.Myelifejotform.dto.ChangeAddress;

import lombok.Data;

import javax.persistence.Column;

@Data
public class ChangeAddressDto {

    private Long id;
    private String changeAddressCode;
    private String policyNo;
    private String policyHolder;
    private String handphoneNo;
    private String emailAddress;
    private boolean isChangeData;
    private String newHandphoneNo;
    private boolean isWhatsappNumber;
    private String whatsappNo;
    private String newEmailAddress;
    private boolean isChangeAddress;
    private String address1;
    private String address2;
    private String address3;
    private String cityCode;
    private String stateCode;
    private String postalCode;
    private String countryCode;
    private String faxNo;
    private String phoneNo;
    private String image;
    private String transactionCode;
    private String transactionStatus;
    private boolean isActive;
}
