package equity.id.Myelifejotform.retrofit;

import lombok.Data;

@Data
public class EditRequest<T> {
    private String username;
    private String password;
    private String app_code;
    private String action;
    private T values;

    public EditRequest() {
        this.username = username;
        this.password = password;
        this.app_code = app_code;
        this.action = action;
        this.values = values;
    }
}
