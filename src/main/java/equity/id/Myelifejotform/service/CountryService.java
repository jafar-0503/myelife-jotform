package equity.id.Myelifejotform.service;

import equity.id.Myelifejotform.config.error.NotFoundException;
import equity.id.Myelifejotform.dto.Country.CountryDto;
import equity.id.Myelifejotform.dto.Country.CreateCountryDto;
import equity.id.Myelifejotform.model.Country;
import equity.id.Myelifejotform.model.State;
import equity.id.Myelifejotform.repository.CountryRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Pageable;
import java.lang.reflect.Type;
import java.util.List;

@Service
public class CountryService {
    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private ModelMapper modelMapper;

    //Get All Data Country
    public ResponseEntity<List<Country>> listCountry(){
        List<Country> countries = countryRepository.findAll();
        Type targetType = new TypeToken<List<Country>>(){}.getType();
        List<Country> response= modelMapper.map(countries, targetType);

        return ResponseEntity.ok(response);
    }

    //Get Data Base on ID
    public ResponseEntity<Country> getCountryById(Long id){
        Country country = countryRepository.findById(id).orElseThrow(()-> new NotFoundException("Data Country ID " + id + " is not exist"));
        Country response = modelMapper.map(country, Country.class);

        return ResponseEntity.ok(response);
    }

    //Post Data Country
    public ResponseEntity<Country> addCountry(Country newCountry){
        Country country = modelMapper.map(newCountry, Country.class);
        Country countrySaved = countryRepository.save(country);
        Country response = modelMapper.map(countrySaved, Country.class);

        return ResponseEntity.ok(response);
    }

    //Update Data Country
    public ResponseEntity<Country> editCountry(Country updateCountry, Long id){
        Country country = countryRepository.findById(id).orElseThrow(()-> new NotFoundException("Data Country ID " + id + " is not exist"));
        country.setCountryCode(updateCountry.getCountryCode());
        country.setDescription(updateCountry.getDescription());
        country.setActive(updateCountry.isActive());
        countryRepository.save(country);

        Country response = modelMapper.map(country, Country.class);
        return ResponseEntity.ok(response);
    }

    //Remove Data Country
    public ResponseEntity<Country> deleteCountry(Long id){
        Country country = countryRepository.findById(id).orElseThrow(()-> new NotFoundException("Data Country ID " + id + " is not exist"));
        countryRepository.deleteById(id);
        Country response = modelMapper.map(country, Country.class);

        return ResponseEntity.ok(response);
    }
}