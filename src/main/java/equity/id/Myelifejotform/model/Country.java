package equity.id.Myelifejotform.model;

import equity.id.Myelifejotform.auditable.Auditable;
import lombok.Data;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "m_country")
@EntityListeners(AuditingEntityListener.class)
public class Country extends Auditable<String> {

    @Column(unique = true, length = 4, name = "country_code")
    private String countryCode;

    @Column(length = 60, name = "description")
    private String description;

    @Column(name = "is_active")
    private boolean isActive;
}