package equity.id.Myelifejotform.model;

import equity.id.Myelifejotform.auditable.Auditable;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Data
@NoArgsConstructor
@Entity
@Table(name = "m_state")
public class State extends Auditable<String> implements Serializable {

    @Column(unique = true, length = 4, name = "state_code")
    private String stateCode;

    @Column(length = 60, name = "description")
    private String description;

    @Column(name = "is_active")
    private boolean isActive;

//    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "state")
//    private Set<ChangeAddress> changeAddressSet;

    public State(String stateCode, String description) {
        this.stateCode = stateCode;
        this.description = description;
    }
}
