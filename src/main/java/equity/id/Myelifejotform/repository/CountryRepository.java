package equity.id.Myelifejotform.repository;

import equity.id.Myelifejotform.model.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {
//    @Query("SELECT c FROM Country c WHERE c.isActive = true")
//    Country findAllByActiveIsTrue(@Param("active") boolean active);

//    List<Country> findAll(String active);

//    @Query("select u from Country u where u.isActive = true")
//    public List<Country> findByActiveIsTrue();


//    Country findByActiveLike(boolean active);

//    @Query("select p from Country p where p.isActive = 1")
////    Optional<Country> findByActiveIsTrueAndId(Long id);
//    List<Country> findAllByActiveOrderByActiveAsc(String status);


}
