package equity.id.Myelifejotform.controller;

import equity.id.Myelifejotform.dto.State.StateDto;
import equity.id.Myelifejotform.dto.State.CreateStateDto;
import equity.id.Myelifejotform.model.State;
import equity.id.Myelifejotform.service.StateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("api/v1")
public class StateController {
    
    @Autowired
    private StateService stateService;

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/states")
    public ResponseEntity<List<State>> listState(){
        return stateService.listState();
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/states/{id}")
    public ResponseEntity<State> getStateById(@PathVariable Long id){
        return stateService.getStateById(id);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/states")
    public ResponseEntity<StateDto> addState(@RequestBody StateDto newState){
        return stateService.addState(newState);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PutMapping("/states/{id}")
    public ResponseEntity<State> editState(@RequestBody State updateState, @PathVariable Long id){
        return stateService.editState(updateState, id);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @DeleteMapping("/states/{id}")
    public ResponseEntity<State> deleteState(@PathVariable Long id){
        return stateService.deleteState(id);
    }
}
